import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MainPart1Component } from './main-part1/main-part1.component';
import { HttpClientModule } from '@angular/common/http';
import { ContactService } from './contact.service';
import { FormsModule } from '@angular/forms';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { MarocComponent } from './maroc/maroc.component';
import { ThailandeComponent } from './thailande/thailande.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './test2/test2.component';
import { TurkeyComponent } from './turkey/turkey.component';
import { EgypteComponent } from './egypte/egypte.component';
import { DubaiComponent } from './dubai/dubai.component';
import { AlgerieComponent } from './algerie/algerie.component';
import { PelerComponent } from './peler/peler.component';
import { NousComponent } from './nous/nous.component';

const routes = [
	{ path: '', component: HomeComponent },
	{ path: 'Contact', component: ContactComponent },
	{ path: 'Thailande', component: ThailandeComponent },
	{ path: 'Maroc', component: MarocComponent },
	{ path: 'test', component: TestComponent },
	{ path: 'test2', component: Test2Component },
	{ path: 'Turkey', component: TurkeyComponent },
	{ path: 'Egypte', component: EgypteComponent },
	{ path: 'UAE', component: DubaiComponent },
	{ path: 'Algerie', component: AlgerieComponent },
	{ path: 'Pelerinag', component: PelerComponent },
	{ path: 'Nous', component: NousComponent }
];

@NgModule({
	declarations: [
		AppComponent,
		MainNavComponent,
		HeaderComponent,
		FooterComponent,
		MainPart1Component,
		ContactComponent,
		HomeComponent,
		MarocComponent,
		ThailandeComponent,
		TestComponent,
		Test2Component,
		TurkeyComponent,
		EgypteComponent,
		DubaiComponent,
		AlgerieComponent,
		PelerComponent,
		NousComponent
	],
	imports: [
		BrowserModule,
		RouterModule.forRoot(routes),
		MaterialModule,
		BrowserAnimationsModule,
		HttpClientModule,
		FormsModule
	],
	providers: [ ContactService, { provide: LocationStrategy, useClass: HashLocationStrategy } ],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
