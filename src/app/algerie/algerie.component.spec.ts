import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlgerieComponent } from './algerie.component';

describe('AlgerieComponent', () => {
  let component: AlgerieComponent;
  let fixture: ComponentFixture<AlgerieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlgerieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlgerieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
