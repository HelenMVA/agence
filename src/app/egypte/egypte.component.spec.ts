import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EgypteComponent } from './egypte.component';

describe('EgypteComponent', () => {
  let component: EgypteComponent;
  let fixture: ComponentFixture<EgypteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EgypteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EgypteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
