import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-egypte',
  templateUrl: './egypte.component.html',
  styleUrls: ['./egypte.component.css']
})
export class EgypteComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);    
  }

}
