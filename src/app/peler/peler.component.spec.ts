import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PelerComponent } from './peler.component';

describe('PelerComponent', () => {
  let component: PelerComponent;
  let fixture: ComponentFixture<PelerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PelerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PelerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
