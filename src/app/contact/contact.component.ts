import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContactService } from '../contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  constructor(
    public contactService: ContactService,
    private httpClient: HttpClient
  ) { }
  
  showMsg: boolean = false;

  contactForm(form) {
    this.contactService.sendContact(form)
      .subscribe(() => {
      console.log('ca marche');
      this.showMsg = true;
    });
  }
  ngOnInit() {
    window.scrollTo(0, 0);
  }
}
