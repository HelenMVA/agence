import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dubai',
  templateUrl: './dubai.component.html',
  styleUrls: ['./dubai.component.css']
})
export class DubaiComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    window.scrollTo(0, 0); /* в начало страницы */
  }
}
