import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThailandeComponent } from './thailande.component';

describe('ThailandeComponent', () => {
  let component: ThailandeComponent;
  let fixture: ComponentFixture<ThailandeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThailandeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThailandeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
