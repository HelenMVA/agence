import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    (function() {
      'use strict';

      function trackScroll() {
        var scrolled = window.pageYOffset;
        var coords = document.documentElement.clientHeight;

        if (scrolled > coords) {
          goTopBtn.classList.add('back_to_top-show');
        }
        if (scrolled < coords) {
          goTopBtn.classList.remove('back_to_top-show');
        }
      }

      function backToTop() {
        if (window.pageYOffset > 0) {
          window.scrollBy(0, -80);
          setTimeout(backToTop, 0);
        }
      }

      var goTopBtn = document.querySelector('.back_to_top');

      window.addEventListener('scroll', trackScroll);
      goTopBtn.addEventListener('click', backToTop);
    })();

    // document.getElementById('apr').onclick = function() {
    //   console.log('aaaaaaaaa');
    //   event.preventDefault();
    //   // document.getElementById('apropos');
    //   document.location.href = 'apropos';
    // };
  }
}
