import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; /* мы делаем для соединения с  */

@Injectable({
	providedIn: 'root'
})
export class ContactService {
	adressAPI = 'https://rayn-voyages.herokuapp.com/send-email';

	constructor(private httpClient: HttpClient) {}
	sendContact(body) {
		return this.httpClient.post(this.adressAPI, body); /* для соединения анг с нод и пеедачи тупо нашего запроса */
	}
}
